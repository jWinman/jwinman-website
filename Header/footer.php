<footer class="footer">

    <div class="footer-left">

        <p class="footer-links">
        <a href="https://www.maths.tcd.ie/~jwinkelm">Home</a>
        ·
        <a href="https://www.maths.tcd.ie/~jwinkelm/Research_interest/index.php">Research interest</a>
        ·
        <a href="https://www.maths.tcd.ie/~jwinkelm/Publications/index.php">Publications</a>
        ·
        <a href="https://www.maths.tcd.ie/~jwinkelm/Teaching/index.php">Teaching</a>
        ·
        <a href="https://www.maths.tcd.ie/~jwinkelm/CV/index.php">CV</a>
        </p>

        <a target="_blank" href="http://research.ie/"><img src="https://www.maths.tcd.ie/~jwinkelm/Images/Longlogo.jpg" alt="IRC-Logo" width="410px" style="padding: 0px 0px 0px 10px"></a>
    </div>

    <div class="footer-center">

        <div>
    	<a target="_blank" href="https://www.google.ie/maps/place/School+of+Physics/@53.343646,-6.2528083,18z/data=!3m1!4b1!4m5!3m4!1s0x48670e90675d940d:0xae9fe1489fb4fa67!8m2!3d53.3436449!4d-6.2520451?hl=de"><i class="fa fa-map-marker"></i></a>
    	<p>
    	<span> The University of Dublin </span>
    	<span> Trinity College </span>
    	<span style="font-size: 16px"> Dublin, Ireland </span> </p>
        </div>

        <div>
    	<a href="mailto:jwinkelm@tcd.ie"><i class="fa fa-envelope"></i></a>
    	<p>jwinkelm@tcd.ie</p>
        </div>

    </div>

    <div class="footer-right">

        <div class="footer-icons">

    	<a target="_blank" href="https://scholar.google.com/citations?user=UKu-95cAAAAJ&hl=de"><i class="ai ai-google-scholar"></i></a>
    	<a target="_blank" href="https://www.linkedin.com/in/jens-winkelmann-685555168/"><i class="fa fa-linkedin"></i></a>
    	<a target="_blank" href="http://arxiv.org:443/find/cond-mat/1/au:+Winkelmann_Jens/0/1/0/all/0/1"><i class="ai ai-arxiv"></i></a>
    	<a target="_blank" href="https://www.researchgate.net/profile/Jens_Winkelmann"><i class="ai ai-researchgate"></i></a>
    	<a target="_blank" href="https://bitbucket.org/jWinman/"><i class="fa fa-bitbucket"></i></a>

        </div>

        <a target="_blank" href="https://www.tcd.ie/Physics/research/groups/foams/"><img src="https://www.maths.tcd.ie/~jwinkelm/Images/foamarms.png" alt="Trinity-Logo" width="223px" style="margin: 0px -13px 0px 0px"></a>

    </div>

</footer>
