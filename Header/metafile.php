<head>
    <TITLE>
    jWinman's page
    </TITLE>
    <meta name="author" content="Jens Winkelmann">
    <meta name="keywords" content="Science, Foam, packings">

    <link href="https://fonts.googleapis.com/css?family=Libre+Baskerville" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <meta name="Jens Winkelmann" content="My personal website" />
    <link href="https://www.maths.tcd.ie/~jwinkelm/Stylefiles/HeaderFooter.css" rel="stylesheet" />
    <link href="https://www.maths.tcd.ie/~jwinkelm/Stylefiles/Article.css" rel="stylesheet" />
    <link href="https://www.maths.tcd.ie/~jwinkelm/Stylefiles/Nav_bar.css" rel="stylesheet" />
    <link href="https://www.maths.tcd.ie/~jwinkelm/Stylefiles/General.css" rel="stylesheet" />
    <link href="https://www.maths.tcd.ie/~jwinkelm/Stylefiles/CV.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.rawgit.com/jpswalsh/academicons/master/css/academicons.min.css">

    <script type="text/javascript" async
      src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-MML-AM_CHTML">
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120937302-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-120937302-1');
    </script>

    <style>
    article {
	font-size: 10;
    }
    </style>

</head>
