<nav>
<ul>
  <li><a href="https://www.maths.tcd.ie/~jwinkelm/index.php"><i class="fa fa-home"></i> Home</a></li>
  <li>
  <div class="dropdown">
    <button class="dropbtn">
      <a href="https://www.maths.tcd.ie/~jwinkelm/Research_interest/index.php">Research interest <i class="fa fa-caret-down"></i></a>
    </button>
    <div class="dropdown-content">
      <a href="https://www.maths.tcd.ie/~jwinkelm/Research_interest/index.php#Soft_Matter">Intro</a>
      <a href="https://www.maths.tcd.ie/~jwinkelm/Research_interest/index.php#Columnar_packings">Columnar packings</a>
      <a href="https://www.maths.tcd.ie/~jwinkelm/Research_interest/index.php#2Dfoam">2D foam simulations</a>
      <a href="https://www.maths.tcd.ie/~jwinkelm/Research_interest/index.php#MPCD">MPC Dynamics</a>
    </div>
  </div>

  </li>
  <li><a href="https://www.maths.tcd.ie/~jwinkelm/Publications/index.php">Publications</a></li>
  <li><a href="https://www.maths.tcd.ie/~jwinkelm/Teaching/index.php">Teaching</a></li>
<!--  <li><a href="https://www.maths.tcd.ie/~jwinkelm/Ireland/index.php">Pics of Ireland</a></li> -->
  <li><a href="https://www.maths.tcd.ie/~jwinkelm/CV/index.php">Curriculum Vitae</a></li>
</ul>
</nav>

<nav class="small">
<ul>
  <li><a href="https://www.maths.tcd.ie/~jwinkelm/index.php"><i class="fa fa-home"></i> Home</a></li>
  <li><a href="https://www.maths.tcd.ie/~jwinkelm/Research_interest/index.php">Research interest</a> </li>
  <li><a href="https://www.maths.tcd.ie/~jwinkelm/CV/index.php">Curriculum Vitae</a></li>
</nav>
