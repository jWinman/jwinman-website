<!DOCTYPE html>
<html>
<?php include_once("Header/metafile.php") ?>

<body>
<div class="container">

<?php include_once("Header/header.php") ?>
<?php include_once("Header/Nav_bar.php") ?>

<aside style="text-align: justify; max-width:30%;">

<h3 style="text-align: center"> Whoami </h3>
<div class="row">
    <div class="2col" style="width:22%; float:left; margin-top:-15px; margin-bottom:-3px;">
    <img src="Images/jens.jpg" width=100% alt="potrait picture">
    </div>
    <div class="2col" style="width:78%; float:right;margin-top:-3px; margin-bottom:6px;">
    <ul>
    <li> <strong>Name:</strong> Jens Winkelmann </li>
    <li> <strong>Email:</strong> jwinkelm@tcd.ie  </li>
    <li> <strong>Location:</strong> SNIAM 3.24  </li>
    </ul>
    </div>
</div>

<p style="font-size: 11px">
Hello there! My name is Jens. I am currently a PhD researcher in the <a target="_blank" href="https://www.tcd.ie/Physics/research/groups/foams">Foams and Complex System Group</a> at Trinity College Dublin (TCD).
</p>
<p style="font-size: 11px">
My research focuses on packing problems and soft matter in general.
<a target="_blank" href="https://en.wikipedia.org/wiki/Soft_matter">Soft matter</a> are squishy materials from foams over liquid crystals to biological system, such as swarms of bacteria.
All of those systems have in common that they are deformable by mechanical or thermal stress.
Applications can be found in everyday products, such as shaving cream, food additives, or even display technology.
</p>
<p style="font-size: 11px">
My work in particular consists mainly of modelling such systems with computer simulations.
Some of my simulation tools are:
</p>
<ul style="font-size: 11px">
<li> <strong> Dev: </strong> Python, C/C++, Bash, git, Surface Evolver </li>
<li> <strong> Parallel: </strong> OpenMP, CUDA </li>
<li> <strong> Libraries: </strong> SciPy, NumPy, Matplotlib, Mayavi, Pandas (Python) Thrust (CUDA) </li>
<li> <strong> IT: </strong> Mac OS, Linux </li>
<li> <strong> Website: </strong> html, css </li>
</ul>

<a class="twitter-timeline" data-width="100%" data-height="400px" data-theme="dark" data-link-color="#19CF86" href="https://twitter.com/WinmanJ?ref_src=twsrc%5Etfw">Tweets by WinmanJ</a>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8">
</script>
</aside>

<article class="article" style="text-align: center; margin-left:75px">
<h1> Willkommen / Welcome / F&aacuteilte </h1>
<p>
Congrats!! You made it to my personal website.
</p>

<p>
If you don't know, whose website you are currently on, look to the right.
You will find a description of who I am.
</p>

<p>
Here, I want to give you an introduction to my research about foam and packing problems.
Have a look around, I hope you learn something and drop me an <a class="email" href="mailto:jwinkelm@tcd.ie">email</a> if you want to get in touch.
</p>

<div class="flex-container">
    <div id="thumbnail">
    <a target="_self" href="Research_interest/index.php">
        <img src="Images/packing.png" alt="Packing" title="Research interest" style="width:140px">
    </a>
    </div>
    <div id="thumbnail">
    <a target="_self" href="Publications/index.php">
        <img src="Images/publications.jpg" alt="Pubs" title="Publications" style="width:155px">
    </a>
    </div>
    <div id="thumbnail">
    <a target="_self" href="Teaching/index.php">
        <img src="Images/schroedinger.jpg" alt="Teaching" title="Teaching" style="width:155px">
    </a>
    </div>
<!--    <div id="thumbnail">
    <a target="_blank" href="Ireland/index.php">
        <img src="Images/Cliffs.jpg" alt="Cliffs" title="Pics of Ireland" style="width:100px">
    </a>
    </div> -->
    <div id="thumbnail">
    <a target="_self" href="CV/index.php">
        <img src="Images/jens_squared.jpeg" alt="CV" title="Curriculum Vitae" style="width:155px">
    </a>
    </div>
</div>

</article>

<article class="article" style="margin-left: 75px">
<h2> A word on Science Hub </h2>
<p>
<q>A spectre is haunting the publishing world of science &ndash; the spectre of <a target="_blank" href="https://en.wikipedia.org/wiki/Sci-Hub">Sci-Hub</a>.
All the powers of old scientific publishers have entered into a holy alliance to excorcise this spectre:
Elsevier, Springer and the American Chemical Society.</q>
</p>

<p>
With this revolutionairy opening, I want to raise awareness to an important issue that should be of concern for everybody.
The majority of all scientific, peer-reviewed publications, are currently hidden behind paywalls because they have been published and sold by companies, such as Elsevier or Springer.
In the last year, these companies have increased their fees for their publications to an scandalous price that even Harvard, the richest university of the global north, stated that they can't afford them any longer <a target="_blank" href="https://www.theguardian.com/science/2012/apr/24/harvard-university-journal-publishers-prices">[1]</a>.
These rising fees come in sharp constrast, to the huge profit margins of the companies, ie. Elsevier had a profit margin of 36% in 2011 <a target="_blank" href="https://svpow.com/2012/01/13/the-obscene-profits-of-commercial-scholarly-publishers/">[2]</a>.
Additionally, authors and reviewers don't get a penny from those companies for their work.
</p>

<p>
With higher and higher journal fees, access to science is prohibited to more and more academics and all non-academics.
As a reaction to this projects such as Library Genesis and Sci-Hub were created.
Both are file-sharing websites that store publications in a repository and offer them for free download.
Especially, Sci-Hub with its over 64.5 million academic papers <a target="_blank" href="https://en.wikipedia.org/wiki/Sci-Hub">[3]</a> plays a major role to make science and knowledge accessible to everybody.
</p>

<p>
Of course, these websites are in a gray area of copyright laws and the big coorperations are using this to fight back.
Elsevier has already won a copyright infringement suit in New York against Library Genesis and Sci-Hub claiming millions of dollars in damage <a target="_blank" href="https://torrentfreak.com/court-orders-shutdown-of-libgen-bookfi-and-sci-hub-151102/">[4]</a>.
And just recently, a Court in Virgina ruled to block Sci-Hub after a lawsuit by the American Chemical Society (ACS) <a target="_blank" href="http://www.sciencemag.org/news/2017/11/court-demands-search-engines-and-internet-service-providers-block-sci-hub?utm_source=sciencemagazine&utm_medium=facebook-text&utm_campaign=scihubblock-16248">[5]</a>.
While Sci-Hub might conflict with current laws, which support the interest of Elsevier and co., it is not in the interest of academia and the public, that knowledge is hidden and unfree.
</p>

<p>
Due to these recent attacks on free knowledge and open access for science, it is time to speak up for these two great projects.
Thus, I want to share with you this <a target="_blank" href="http://custodians.online/">letter</a> in solidarity with Library Genesis and Sci-Hub.
I encourage you to read it and share it as well.
</p>

<p>

</p>
</article>
<?php include_once("Header/footer.php") ?>
</div>
</body>
</html>
