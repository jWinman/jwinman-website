<!DOCTYPE html>
<html>
<?php include_once("../Header/metafile.php") ?>

<body>
<div class="container">

<?php include_once("../Header/header.php") ?>
<?php include_once("../Header/Nav_bar.php") ?>

<nav class="side">
<ul>
  <li><a href="https://www.maths.tcd.ie/~jwinkelm/Teaching/index.php#Teaching">Teaching</a>
  <hr class="navbar">
  </li>
</ul>
</nav>

<article class="article">
<h1 id="Teaching"> Teaching </h1>

<h3> Trinity College Dublin </h3>
<ul style="list-style-type: none">
<li> <strong> Full Term 2018/19</strong> &ndash; Supervision of Final Year Project &ndash; <q>Disk packing inside a 2D channel</q> (J. Ryan-Purcell)
<li> <strong> Hilary Term 2019</strong> &ndash; Tutor for <q>MA3443: Statistical Physics II</q> (Manuela Kulaxizi) </li>
<li> <strong> Hilary Term 2019</strong> &ndash; Tutor for <q>MA1242: Mechanics II</q> (Jan Manschot) </li>
<li> <strong> Michaelmas Term 2018/19</strong> &ndash; Tutor for <q>MA3443: Statistical Physics I</q> (Manuela Kulaxizi) </li>
<li> <strong> Full Term 2017/18</strong> &ndash; Supervision of Final Year Project &ndash; <q>Columnar packings in a quad. potential</q> (D.B. Williams)
<li> <strong> Hilary Term 2018</strong> &ndash; Tutor for <q>MA1242: Mechanics II</q> (Jan Manschot) </li>
<li> <strong> Hilary Term 2018</strong> &ndash; Laboratory teaching &ndash; JF Engineering and JF Science/N-PCAM/CMM </li>
<li> <strong> Michaelmas Term 2017/18</strong> &ndash; Python lab teaching &ndash; <q>PY3C01: Computer Simulations I</q> (Stefan Hutzler) </li>
<li> <strong> Michaelmas Term 2017/18</strong> &ndash; Tutor for <q>MA1M01: Mathematical Methods for Life Science Students</q> (Sinéad Ryan) </li>
<li> <strong> Hilary Term 2017</strong> &ndash; Laboratory teaching &ndash; JF Engineering and JF Science/N-PCAM/CMM </li>
<li> <strong> Hilary Term 2017</strong> &ndash; Tutor for <q>MA1S12: Math for Scientist</q> (Colm Ó Dúnlaing) </li>
<li> <strong> Michaelmas Term 2016/17</strong> &ndash; Tutor for <q>MA1M01: Mathematical Methods for Life Science Students</q> (Joe Ó hÓáin) </li>
<li> <strong> Hilary Term 2015</strong> &ndash; Laboratory teaching &ndash; JF Theoretical Physics </li>
<li> <strong> Michaelmas Term 2015/16</strong> &ndash; Supervision of 2nd Year Poster Project &ndash; <q>The Pursuit of People Packing</q>
<li> <strong> Michaelmas Term 2015/16</strong> &ndash; Supervision of Final Year Project &ndash; <q>Soft Sphere Packing</q> (S. Lahert & J. Roche)
</ul>

<h3> Technische Universit&auml;t Dortmund </h3>
<ul style="list-style-type: none">
<li> <strong> SS2015 </strong> &ndash; Übungsgruppenleiter für <q>Computational Physics</q> (Kierfeld) </li>
<li> <strong> WS2014/15 </strong> &ndash; Übungsgruppenleiter für <q>Theoretische Physik für Medizinphysiker</q> (Löw) </li>
<li> <strong> SS2014 </strong> &ndash; Übungsgruppenleiter für <q>Computational Physics</q> (Löw) </li>
<li> <strong> WS2013/14 </strong> &ndash; Übungsgruppenleiter für <q>Einführung in die Festkörperphysik</q> (Böhmer) </li>
<li> <strong> WS2013/14 </strong> &ndash; Übungsgruppenleiter für <q>Mathematischer Vorkurs der Fakultät Physik</q> (Stolze/Tolan) </li>
</ul>

</article>

<?php include_once("../Header/footer.php") ?>
</div>
</body>
</html>
