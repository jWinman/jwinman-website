<!DOCTYPE html>
<html>
<?php include_once("../Header/metafile.php") ?>

<body>
<div class="container">

<?php include_once("../Header/header.php") ?>
<?php include_once("../Header/Nav_bar.php") ?>

<article class="cv" style="margin-left: 75px">

<div id="page-wrap">

    <img src="../Images/jens_squared2.jpeg" alt="Photo of Me" id="pic" />

    <div id="contact-info" class="vcard">
        <!-- Microformats! -->

        <h1 class="cv">Jens Winkelmann</h1>

        <p class="cv">
            Email: <a class="email" href="mailto:jwinkelm@tcd.ie">jwinkelm@tcd.ie</a>
            <ul class="cv-symbols">
		<li class="cv-symbols"> <a target="_blank" href="https://scholar.google.com/citations?user=UKu-95cAAAAJ&hl=de"><i class="ai ai-google-scholar"></i></a> </li>
		<li class="cv-symbols"> <a target="_blank" href="https://www.linkedin.com/in/jens-winkelmann-685555168/"><i class="fa fa-linkedin"></i></a></li>
		<li class="cv-symbols"> <a target="_blank" href="http://arxiv.org:443/find/cond-mat/1/au:+Winkelmann_Jens/0/1/0/all/0/1"><i class="ai ai-arxiv"></i></a> </li>
		<li class="cv-symbols"> <a target="_blank" href="https://www.researchgate.net/profile/Jens_Winkelmann"><i class="ai ai-researchgate"></i></a> </li>
		<li class="cv-symbols"> <a target="_blank" href="https://bitbucket.org/jWinman/"><i class="fa fa-bitbucket"></i></a></li>
            </ul>
        </p>
    </div>

    <div id="objective">
        <p class="cv">
            <a href="Jens-Winkelmann-CV.pdf">A pdf version of my CV can be downloaded from here.</a>
        </p>
        <p class="cv">
	    Packing your luggage before you go on holidays or taking a jammed packed bus to work are just two occurrences of packing problems in our everyday life.
	    During my PhD at Trinity College Dublin, funded by the Irish Research Council, I focussed on such <strong> packing problems </strong> by employing optimisation algorithms.
        </p>
        <p class="cv">
            Previously, I pursuit a three-year Bachelor followed by a two-year Master in Physics at TU Dortmund, Germany.
    	    Throughout my studies, I gained teaching experience at Trinity College as well as TU Dortmund and worked as a summer student at Forschungszentrum Jülich, Germany.
       </p>
       <p class="cv">
            I will finish my PhD by September 2019.
            So after that I am seeking a challenging job that fits my professional skills and personal interests.
       </p>
    </div>

    <div class="clear"></div>

    <dl>
        <dd class="clear"></dd>

        <dt class="cv">Experience</dt>
        <dd class="cv">
            <h2 class="cv"> Trinity College Dublin <span> PhD Researcher &ndash; 10/2015-now </span></h2>
            <ul class="cv">
              <li class="cv">Solving <strong> packing problems </strong>  with optimisation algorithms in C/C++ or Python</li>
              <li class="cv"><strong>Web developer</strong> for <a target="_blank" href="http://www.tcd.ie/Senior_Tutor">Senior Tutor Service</a> and <a target="_blank" href="https://www.tcd.ie/Physics/Research/Groups/Foams/">Foams Group</a> </li>
              <li class"cv"><strong>Lab demonstrator and tutor</strong> for <strong> 6</strong> physics teaching labs and <strong> 7 </strong> mathematical lectures</li>
              <li class"cv"><strong>Supervisor</strong> of <strong> 4</strong> undergraduate student projects</li>
            </ul>
            <p class="cv"> <strong> 4 </strong> peer-reviewed <a href="https://www.maths.tcd.ie/~jwinkelm/Publications/index.php#Pubs">publications</a> and <strong> 9 </strong> <a href="https://www.maths.tcd.ie/~jwinkelm/Publications/index.php#Conferences">posters/presentations</a>

            <h2 class="cv"> TU Dortmund <span> Übungsgruppenleiter (Tutor) &ndash; 10/2013-09/2015 </span></h2>
            <strong>Tutored lectures from the Physics Department:</strong>
            <ul class="cv">
              <li class="cv"> Computational physics </li>
              <li class="cv"> Theoretical physics I </li>
              <li class="cv"> Introduction to condensed matter </li>
              <li class="cv"> Mathematical pre-lectures</li>
            </ul>
            <p class="cv"> Experience in public speaking and presentational skills </p>

            <h2 class="cv"> Forschungszentrum Jülich <span> Summer student &ndash; 07/2014-10/2014 </span></h2>
            <strong>IHRS BioSoft Guest Student:</strong> Institute of Complex Systems 2<br />
            <ul class="cv">
              <li class="cv">Running simulations on Linux-based High Performance Computers</li>
              <li class="cv">Data analysis with Python</li>
              <li class="cv">Data visualisation with Matplotlib/Python</li>
            </ul>
            <p><strong>Research project:</strong> An active dumbbell in a mesoscopic hydrodynamic simulation</p>
        </dd>
        <dd class="clear"></dd>

        <dt class="cv">Education</dt>
        <dd class="cv">
            <h2 class="cv">Trinity College Dublin <span>PhD Student &ndash; 10/2015-2019 </span></h2>
            <p class="cv"><strong>PhD Student:</strong> Foams and Complex System Group, School of Physics<br />
               <strong>Working title:</strong> Models of wet foams and other systems of soft particles</p>
            <h2 class="cv">TU Dortmund <span>Undergrad and Postgrad student &ndash; 2010-2015 </span></h2>
            <p class="cv">
               <strong>Master of Science in Physics:</strong> Overall grade 1.2 <br />
            <span>
               <strong> Master thesis: </strong> <a target="_blank" href="../Files/report.pdf">Hydrodynamic Interactions between Polymers in a Mesoscopic Simulation</a><br />
            </span>
               <strong>Bachelor of Science in Physics: </strong> Overall grade 1.4 <br />
            <span>
               <strong>Bachelor thesis: </strong> Mulit-particle collision dynamics for simulating active particles
            </span>
            </p>
            <h2 class="cv">Don-Bosco Gymnasium Essen <span>Secondary school &ndash; 2001-2010 </span></h2>
            <p class="cv"><strong>Leaving certificate / Abitur:</strong> Overall grade 1.2</p>
            <h2 class="cv">Northwest Rankin High School <span> Exchange Year &ndash; 09/2007-07/2008</span></h2>
            <p class="cv"> <strong>High School Year</strong> in Brandon, Mississippi, USA </p>
        </dd>
        <dd class="clear"></dd>

        <dt class="cv">Awards</dt>
        <dd class="cv">
            <h2 class="cv">IRC Postgraduate Scholarship<span>10/2015-09/2019</span></h2>
            <p class="cv"> <strong> Funding scheme:</strong> Irish Research Council Postgraduate Scholarship <br />
               <span> (215 awardees out of 1200 eligible applications) </span>
               <strong> Project title:</strong> Models of wet foams and other systems of soft particles
            </p>
            <h2 class="cv">Poster Prize at Cambridge University, UK <span>08/09/2017</span></h2>
            <p class="cv"><strong>Soft Matter Poster Prize</strong> at the <a href="https://www.turing-gateway.cam.ac.uk/event/tgmw43">2nd Edwards Symposium: Challenges and Opportunities in Soft Matter</a> (<a target="_blank" href="../Files/CambridgeJens.pdf">Poster</a>)</p>
        </dd>
        <dd class="clear"></dd>

        <dt class="cv">Certificates</dt>
        <dd class="cv">
            <h2 class="cv"> IELTS English Test<span>10/03/2016</span></h2>
            <p class="cv"><strong>Overall Band Score:</strong> 7.5 / 9.0 <br />
               <strong>CEFR Level: </strong> C1</p>
        </dd>
        <dd class="clear"></dd>

        <dt class="cv">Conferences and Workshops</dt>
        <dd class="cv">
            <h2 class="cv"> Conferences </h2>
            <ul class="cv">
                <li> WE-Heraeus Symposium: Science on the ISS, Bad Honnef (2018) </li>
                <li> 3rd Edwards Symposium, Cambridge (2018) </li>
                <li> Eufoam, Liege (2018) </li>
                <li> Flowing Matter, Lisbon (2018) </li>
                <li> 2nd Edwards Symposium, Cambridge (2017) </li>
                <li> PyCon Ireland, Dublin (2016) </li>
                <li> Eufoam, Dublin (2016) </li>
            </ul>
            <h2 class="cv"> Workshops </h2>
            <ul class="cv">
                <li> CISM: Mechanics of Liquid and Solid Foams, Udine (2017) </li>
                <li> Workshop on Jamming and Granular Matter, London (2016) </li>
                <li> 46th IFF Spring School, Jülich (2015) </li>
                <li> 45th IFF Spring School, Jülich (2014) </li>
            </ul>
        </dd>
        <dd class="clear"></dd>
    </dl>

    <div class="clear"></div>

</div>

</article>


<?php include_once("../Header/footer.php") ?>
</div>
</body>
</html>
