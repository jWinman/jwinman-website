<!DOCTYPE html>
<html>
<?php include_once("../Header/metafile.php") ?>

<body>
<div class="container">

<?php include_once("../Header/header.php") ?>
<?php include_once("../Header/Nav_bar.php") ?>

<article class="article" style="text-align: center; margin-left:75px">
<h2 id="Transitions"> Boundaries of the phase diagram </h2>

<video  height="470" width="750" controls autoplay loop>
        <source src="TransitionTypes.mp4">
        <p class="warning">Your browser does not support HTML5 video.</p>
</video>
</article>

<article class="article" style="text-align: center; margin-left:75px">
<h2 id="Rotational_hard_spheres"> Lathe experiments with (hard) polymeric beads </h2>
Lee <i>et al.</i> Advanced Materials 29, 1704274 (2017)
<video  height="470" width="750" controls autoplay loop>
        <source src="HardSpheresMovie.mp4">
        <p class="warning">Your browser does not support HTML5 video.</p>
</video>
<h2> \(\omega = 0\) to \(3000\) rpm</h2>
</article>

<article class="article" style="text-align: center; margin-left:75px">
<h2 id="Rotational_soft_spheres"> Lathe experiments with (soft) hydrogel spheres </h2>
<video  height="470" width="750" controls autoplay loop>
        <source src="SoftSpheres1.mp4">
        <p class="warning">Your browser does not support HTML5 video.</p>
</video>
<h2> \(N = 85\) and \(\omega = 1802\) rpm</h2>

<video  height="470" width="750" controls autoplay loop>
        <source src="SoftSpheres2.mp4">
        <p class="warning">Your browser does not support HTML5 video.</p>
</video>
<h2> \(N = 60\) and \(\omega = 1821\) rpm</h2>
</article>

<?php include_once("../Header/footer.php") ?>
</div>
</body>
</html>
