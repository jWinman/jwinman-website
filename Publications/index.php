<!DOCTYPE html>
<html>
<?php include_once("../Header/metafile.php") ?>

<body>
<div class="container">

<?php include_once("../Header/header.php") ?>
<?php include_once("../Header/Nav_bar.php") ?>

<nav class="side">
<ul>
  <li><a href="https://www.maths.tcd.ie/~jwinkelm/Publications/index.php#Pubs">Publications</a>
  <hr class="navbar">
  </li>
  <li><a href="https://www.maths.tcd.ie/~jwinkelm/Publications/index.php#Conferences">Conferences</a>
  <hr class="navbar">
  </li>
</ul>
</nav>

<article class="article">
<h1 id="Pubs"> Publications </h1>

<h3> 2018 </h3>
<p>
<b> 4: </b> Winkelmann J., Mughal A., Williams D.B., Weaire D., Hutzler S. (2018), <a target="_blank" href="https://arxiv.org/abs/1808.02952">Theory of rotational columnar structures of soft spheres</a>, <i> Phys. Rev. Letters </i>, <b>  </b> status: <i>submitted</i>
</p>

<p>
<b> 3: </b> Mughal A., Winkelmann J., Weaire D., Hutzler S. (2018), <a target="_blank" href="https://arxiv.org/abs/1805.07673">Columnar structures of soft spheres: metastability and hysteresis</a> (<a target="_blank" href="https://www.maths.tcd.ie/~jwinkelm/Publications/SM-PhysRevE-982018">Suppl. Mat.</a>), <i> Phys. Rev. E </i>, <b> 98 </b> 043303
</p>

<h3> 2017 </h3>

<p>
<b> 2: </b> Winkelmann J., Dunne F.F., Langlois V.J., M&ouml;bius M.E., Weaire D., Hutzler S. (2017) <a target="_blank" href="https://arxiv.org/abs/1708.06980">2d foams above the jamming transition: Deformation matters</a>, <i> Colloids and Surfaces A: Physicochemical and Engineering Aspects </i> <b> 534 </b> 52&ndash;57
</p>

<p>
<b> 1: </b> Winkelmann J., Haffner B., Weaire D., Mughal A., Hutzler S. (2017), <a target="_blank" href="https://arxiv.org/abs/1703.00773">Simulation and observation of line-slip structures in columnar structures of soft spheres</a>, <i> Phys. Rev. E </i> <b> 96 </b> 012610
</p>

</article>

<article class="article">
<h1 id="Conferences"> Conference contributions </h1>

<h3> 2018 </h3>
<p>
<b> 8: </b> Winkelmann J., Mughal A., Williams D.B., Weaire D., Hutzler S. (2018) <a target="_blank" href="../Files/Cambridge2018.pdf">Theory of rotational columnar structures of soft spheres</a> (<a target="_blank" href="../Files/Cambridge2018Pitch.pdf">elevator pitch</a>), 3rd Edwards Symposium. Challenges and Opportunities in Soft Matter 2018, Cambridge (UK).
</p>

<p>
<b> 7: </b> Winkelmann J., Mughal A., Haffner B., Weaire D., Hutzler S. (2018) <a target="_blank" href="../Files/LiegePresentation.pdf">Columnar packings of soft spheres</a>, Eufoam 2018, Liége (Belgium).
</p>

<p>
<b> 6: </b> Winkelmann J., <a target="_blank" href="https://www.kymcox.com">Cox K.</a>, Kraynik A., Reinelt D.A., Hutzler S. (2018) <a target="_blank" href="../Files/EufoamLiege.pdf">Columnar bubble chains</a>, Eufoam 2018, Liége (Belgium).
</p>

<p>
<b> 5: </b> Winkelmann J., Williams D., Mughal A., Weaire D., Hutzler S. (2018) <a target="_blank" href="../Files/PosterLisbon.pdf">Columnar packings of soft spheres in rotational fluids</a>, Flowing Matter 2018, Lisbon (Portugal).
</p>

<h3> 2017 </h3>

<p>
<b> 4: </b> Winkelmann J., Dunne F.F., Lanlois V.J., Möbius M.E., Weaire D., Hutzler S. (2017) <a target="_blank" href="../Files/CambridgeJens.pdf">2D foams above the jamming transition: Deformation matters</a> (<a target="_blank" href="../Files/CambridgePresentation.pdf">elevator pitch</a>), 2nd Edwards Symposium. Challenges and Opportunities in Soft Matter, Cambridge (UK).
</p>

<p>
<b> 3: </b> Winkelmann J., Dunne F.F., Weaire D., Höhler R., Hutzler S. (2017) <a target="_blank" href="../Files/CambridgeDenis.pdf">Morse-Witten theory and its applications</a>, 2nd Edwards Symposium. Challenges and Opportunities in Soft Matter, Cambridge (UK).
</p>

<h3> 2016 </h3>
<p>
<b> 2: </b> Winkelmann J., Dunne F.F., Möbius M.E., Weaire D., Hutzler S. (2016) <a target="_blank" href="../Files/London.pdf">2D foams above the jamming transition: Deformation matters</a>, International Workshop on Jamming and Granular Matter, London (UK).
</p>

<p>
<b> 1: </b> Winkelmann J., Lahert S., Roche J., Mughal A., Weaire D., Hutzler S. (2016) <a target="_blank" href="../Files/Eufoam.pdf">Packing of bubbles and soft spheres in cylindrical confinement</a>, Eufoam 2016 Conference, Dublin (Ireland).
</p>

</article>

<?php include_once("../Header/footer.php") ?>
</div>
</body>
</html>
